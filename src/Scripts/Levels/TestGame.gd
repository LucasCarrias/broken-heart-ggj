extends Node2D

onready var player = get_node("Player")
onready var boss = get_node("NPCs/Boss")

func _ready():
	dream_of_trump(50)

func draw_aim():
	$Aim.rotation = boss.get_angle_to(player.position)
	$Aim.exclude_parent = true
	$Aim.position = boss.position
	$Aim.cast_to = Vector2(boss.position.distance_to(player.position), 0)
	$Aim.enabled = true


func _on_Player_dash(player_pos):
	var dash_fx = preload("res://src/Scenes/Effects/DashFx.tscn").instance()
	dash_fx.global_position = player_pos
	add_child(dash_fx)

func dream_of_trump(value):
	var block = $Block
	for i in range(value):
		var new1 = block.duplicate()
		var new2 = block.duplicate()
		var new3 = block.duplicate()
		var new4= block.duplicate()
		new1.position = Vector2(32*i, 0)
		new2.position = Vector2(32*i, 32*value)
		new3.position = Vector2(0, 32*i)
		new4.position = Vector2(32*value, 32*i)
		add_child(new1)
		add_child(new2)
		add_child(new3)
		add_child(new4)
		
	


func _on_Boss_target_player():
	draw_aim()
	var boss_pos = $NPCs/Boss.global_position
	var player_pos = $Player.global_position
	var img = preload("res://src/Assets/Objects/Hit/HitLine.tscn").instance()
	img.rect_position = $Aim.global_position
	img.rect_rotation = $Aim.rotation_degrees	
	img.rect_size.x = boss_pos.distance_to(player_pos)
	
	print(img.rect_position)
	add_child(img)
