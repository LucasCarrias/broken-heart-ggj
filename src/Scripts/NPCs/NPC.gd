extends StaticBody2D

var player_nearby = false
onready var dialogBox = get_node("../../Player/HUD/DialogBox")
onready var player = get_node("../../Player")
onready var sentences = [] setget set_sentences
onready var confirmationBox = get_node("ConfirmationDialog")
var current_index = 0
var confirmQuestion = ""
var callDialog = false

func _physics_process(delta):
	if player_nearby and Input.is_action_just_pressed("ui_accept") and not dialogBox.typing:
		if current_index < len(sentences):
			dialogBox.start(sentences[current_index])
			current_index += 1
			player.set_move(false)
		else:
			player.set_move(true)
			dialogBox.hide()
			current_index = 0
			if callDialog:
				get_response()
			

func call_confirmation(question):
	confirmQuestion = question
	callDialog = true

func get_response():
	confirmationBox.dialog_text = confirmQuestion
	confirmationBox.show()

func set_sentences(dialogs):
	sentences = dialogs

func _on_TalkArea_body_entered(body):
	if body.name == "Player":
		_set_dialog_by_progress()
		player_nearby = true


func _on_TalkArea_body_exited(body):
	if body.name == "Player":
		player_nearby = false


func _set_dialog_by_progress():
	pass


func _resolve_dialog():
	pass

func _on_ConfirmationDialog_confirmed():
	_resolve_dialog()
	callDialog = false
