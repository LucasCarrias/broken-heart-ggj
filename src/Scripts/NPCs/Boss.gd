extends KinematicBody2D

signal target_player

var health = 100

onready var player = get_node("../../Player")
onready var screen_shake = get_node("../../Player/Camera2D/ScreenShake")
onready var aim = get_parent().get_parent()
onready var seeking = false
onready var available_mechanics = ["seek_and_drop", "random_movement", "aim_and_shoot", "meteor_shower"]
onready var allowed_mechanics = []
onready var world = get_tree().current_scene


# MECHANICS ALLOWERS
onready var seek_and_drop = false
onready var random_movement = false
onready var aim_and_shoot = false
onready var meteor_shower = false
onready var show_line = false

var is_showing_line = false

const SHOOT = preload("res://src/Scenes/Objects/Shoot.tscn")
const DROPZONE = preload("res://src/Scenes/Objects/DropZone.tscn")
const SHADOW_METEOR = preload("res://src/Scenes/Objects/ShadowMeteor.tscn")
const METEOR = preload("res://src/Scenes/Objects/Meteor.tscn")

var aiming = false
var aiming_shoot = false
var meteor_allowed = true
var meteor_counter = 0
var next_drop = Vector2()

func _ready():
	_set_allowed_mechanics([
		"seek_and_drop",
		"show_line",
		"random_movement",
		"aim_and_shoot",
		"meteor_shower"
		])
	$RestTiime.start()

func _physics_process(delta):
	call_allowed_mechanics()

func call_allowed_mechanics():
	if seek_and_drop:
		seek_and_drop_loop()
	elif random_movement:
		random_movement()
	elif aim_and_shoot:
		aim_and_shoot()
	elif meteor_shower:
		meteor_shower()
	elif show_line:
		
		show_line()

func _set_allowed_mechanics(mechanics_list) -> void:
	allowed_mechanics = mechanics_list


func seek_and_drop_loop() -> void:
	set_next_drop()	
	if not seeking:
		aiming = true
		seeking = true
		$WaitForNextSeek.start()

func set_next_drop() -> void:
	if aiming:
		next_drop = player.position
		aim.draw_aim()


func get_random_int(min_value, max_value) -> int:
	if max_value == 0: max_value += 0.1
	randomize()
	return (randi() % max_value) + min_value


func random_movement() -> void:
	random_movement = false
	var distance = 100
	var random_dir = get_random_int(1, 8)
	match random_dir:
		1:
			$RandomMoveTween.interpolate_property(self, "position", null, Vector2(position.x, position.y - distance), 1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
		2:
			$RandomMoveTween.interpolate_property(self, "position", null, Vector2(position.x + distance, position.y - distance), 1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
		3:
			$RandomMoveTween.interpolate_property(self, "position", null, Vector2(position.x + distance, position.y), 1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)			
		4:
			$RandomMoveTween.interpolate_property(self, "position", null, Vector2(position.x + distance, position.y + distance), 1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
		5:
			$RandomMoveTween.interpolate_property(self, "position", null, Vector2(position.x, position.y + distance), 1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)			
		6:
			$RandomMoveTween.interpolate_property(self, "position", null, Vector2(position.x - distance, position.y + distance), 1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
		7:
			$RandomMoveTween.interpolate_property(self, "position", null, Vector2(position.x - distance, position.y), 1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)			
		8:
			$RandomMoveTween.interpolate_property(self, "position", null, Vector2(position.x - distance, position.y - distance), 1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)

	$RandomMoveTween.start()


func _on_HeightTweenUp_tween_all_completed():
	$HeightTweenDown.interpolate_property(self, "scale", null, Vector2(scale.x - 1, scale.y - 1), 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
	$HeightTweenDown.start()


func _on_HeightTweenDown_tween_all_completed():
	var new_drop_zone = DROPZONE.instance()
	world.add_child(new_drop_zone)
	new_drop_zone.position = Vector2(self.position.x, self.position.y + 40)
	#screen_shake.start()
	$RestTiime.start()


func _on_WaitForNextSeek_timeout():
	$MovementTween.interpolate_property(self, "position", null, next_drop, 0.5, Tween.TRANS_SINE, Tween.EASE_OUT)
	$HeightTweenUp.interpolate_property(self, "scale", null, Vector2(scale.x + 1, scale.y + 1), 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
	$MovementTween.start()
	$HeightTweenUp.start()
	seeking = false
	aiming = false
	seek_and_drop = false
	show_line = false


func execute_random_mechanic() -> void:
	var random_mec = allowed_mechanics[get_random_int(0, len(allowed_mechanics))]
	print(random_mec)
	match random_mec:
		"seek_and_drop":
			seek_and_drop = true
		"random_movement":
			random_movement = true
		"aim_and_shoot":
			aim_and_shoot = true
		"meteor_shower":
			meteor_shower = true
		"show_line":
			is_showing_line = false			
			show_line = true
			


func _on_RestTiime_timeout():
	execute_random_mechanic()


func _on_RandomMoveTween_tween_all_completed():
	random_movement = false
	if meteor_counter == 0: $RestTiime.start()


func aim_and_shoot():
	set_next_drop()	
	if not aiming_shoot:
		aiming = true
		aiming_shoot = true
		$AimTimer.start()


func _on_AimTimer_timeout():
	aim_and_shoot = false
	aiming = false
	aiming_shoot = false
	var this_shoot = SHOOT.instance()
	world.add_child(this_shoot)
	var variant = 1 #get_random_int(1, 2)
	var target = Vector2()
	if variant == 1:
		this_shoot.set_drop(true)
		this_shoot.shoot()
		target = next_drop
	else:
		target = Vector2(next_drop.x + 1000, next_drop.y + 1000)
	$ShootTween.interpolate_property(this_shoot, "position", self.position, target, 0.2, Tween.TRANS_SINE, Tween.EASE_OUT)
	$ShootTween.start()
	

func _on_ShootTween_tween_all_completed():
	$RestTiime.start()
	

func meteor_shower():	
	if meteor_allowed:		
		meteor_counter += 1
		meteor_allowed = false
		var random_pos = Vector2(get_random_int(int(player.position.x - 50), int(player.position.x + 50)), get_random_int(int(player.position.y - 50), int(player.position.y + 50)))
		summon_meteor(random_pos)
		random_movement()


func summon_meteor(origin_pos):
	var new_shadow = SHADOW_METEOR.instance()
	world.add_child(new_shadow)
	new_shadow.position = origin_pos
	drop_meteor(new_shadow.position)


func drop_meteor(drop_pos):
	var new_meteor = METEOR.instance()
	world.add_child(new_meteor)
	new_meteor.drop(Vector2(drop_pos.x, drop_pos.y - 500))
	if meteor_counter <= 9:
		$NextShadowMeteor.wait_time = get_random_int(1, 3)
		$NextShadowMeteor.start()
	else:
		$RestTiime.start()
		meteor_counter = 0
		meteor_allowed = true


func _on_HitBox_body_entered(body):
	if body.name == "Player":
		player.hitted(5)


func _on_NextShadowMeteor_timeout():
	meteor_allowed = true

func show_line():
	if !is_showing_line:
		is_showing_line = true
		emit_signal("target_player")
		$RestTiime.start()
	

func _on_NextMeteor_timeout():
	pass # Replace with function body.


func consume_health(value):
	$HealthBar.modulate.a = 1
	health = clamp(health - value, 0, 100)

func _on_HealthFade_timeout():
	$HealthBar.modulate.a = lerp($HealthBar.modulate.a, 0, 0.10)
