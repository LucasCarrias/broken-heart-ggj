extends Sprite

signal consume_stamina

var power = 5
var initial_color = modulate.a

func _ready():
	pass
	
func _process(delta):
	pass
	
func on():
	if $Timer.is_stopped():
		visible = true
		$Timer.start()

func off():
	$Timer.stop()
	visible = false

func _on_Timer_timeout():
	emit_signal("consume_stamina", power)
