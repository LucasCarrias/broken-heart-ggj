extends KinematicBody2D

signal death
signal dash

const WALK_SPEED = 150
const RUN_SPEED = 700
const HAMMER_SHADOW = preload("res://src/Scenes/Objects/HammerShadow.tscn")
const HAMMER_EXPLOSION = preload("res://src/Scenes/Objects/HammerExplosion.tscn")

var current_speed = 150
var movedir = Vector2(0,0)
var last_dir = Vector2.ZERO
var can_move = true setget set_move
var can_dash = true
var dashing = false
var idle_frame = 0
var hammer_drop_pos = Vector2()

var health = 100
var stamina = 100

var shield_up = false

func _ready():
	$HealthBar.modulate.a = 0
	$HealthBar.value = health

func _physics_process(delta):
	$HealthBar.value = lerp($HealthBar.value, health, 0.3)
	$StaminaBar.value = lerp($StaminaBar.value, stamina, 0.3)
	if can_move:
		check_life()
		controls_loop()
		movement_loop()
		updade_anim()
		toggle_shield()
	if shield_up:
		$Shield.on()	
	else:
		$Shield.off()


func check_life():
	if health <= 0:
		emit_signal("death")


func controls_loop():
	var LEFT = Input.is_action_pressed("ui_a")
	var RIGHT = Input.is_action_pressed("ui_d") 
	var UP = Input.is_action_pressed("ui_w") 
	var DOWN = Input.is_action_pressed("ui_s")

	movedir.x = -int(LEFT) + int(RIGHT)
	movedir.y = -int(UP) + int(DOWN)
	if movedir != Vector2.ZERO:
		last_dir= movedir
	
	attack_control()


func movement_loop():
	if Input.is_action_pressed("ui_shift") and can_dash:
		consume_stamina(1)
		if not dashing: 
			$DashTimer.start()
			dashing = true
			emit_signal("dash", global_position)
	else:
		current_speed = WALK_SPEED

	if dashing:
		current_speed += 100

	var motion = movedir.normalized() * current_speed
	move_and_slide(motion, Vector2(0,0))


func updade_anim():
	if movedir == Vector2.ZERO:
		if last_dir.x != 0:
			$AnimatedSprite.play("idle_side")
		elif last_dir.y != 0:
			if last_dir.y == -1:
				$AnimatedSprite.play("idle_back")
			elif last_dir.y == 1:
				$AnimatedSprite.play("idle")
	elif movedir.x != 0:
		if movedir.x == 1:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
		$AnimatedSprite.play("walk_side")
	elif movedir.y != 0:
		if movedir.y == 1:
			$AnimatedSprite.play("walk_front")
		elif movedir.y == -1:
			$AnimatedSprite.play("walk_back")


func attack_control():
	if Input.is_action_pressed("ui_mouse_left"):
		charge_hammer()
	elif Input.is_action_just_released("ui_mouse_left"):
		drop_hammer()


func charge_hammer():
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	var new_shadow = HAMMER_SHADOW.instance()
	get_tree().current_scene.add_child(new_shadow)
	var mouse = get_global_mouse_position()
	new_shadow.position = Vector2(
		clamp(mouse.x, self.position.x - 200, self.position.x + 200), 
		clamp(mouse.y, self.position.y - 200, self.position.y + 200))
	$HammerBase/Hammer.visible = true
	$HammerBase/Hammer.scale += Vector2(0.01 * 0.1, 0.01 * 0.1)
	hammer_drop_pos = new_shadow.position


func drop_hammer():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$HammerTween.interpolate_property($HammerBase/Hammer, "global_position", null, hammer_drop_pos, 0.2, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
	$HammerTween.start()


func _on_HammerTween_tween_all_completed():
	var new_explosion = HAMMER_EXPLOSION.instance()
	get_tree().current_scene.add_child(new_explosion)
	new_explosion.drop(hammer_drop_pos, $HammerBase/Hammer.scale * 25)
	$HammerBase/Hammer.visible = false
	$HammerBase/Hammer.scale = Vector2(0.01, 0.01)
	$HammerBase/Hammer.position = Vector2($HammerBase.position.x, $HammerBase.position.y -175)
	


func rotate_pointer(point_direction: Vector2)->void:
	var temp = rad2deg(atan2(point_direction.y, point_direction.x))
	$DashRay.rotation_degrees = temp
	

func hitted(damage: float):
	if shield_up:
		consume_stamina(damage)
	else:
		health = clamp(health - damage, 0, 100)
	$HealthBar.modulate.a = 1
	$HitTween.interpolate_property($AnimatedSprite, "modulate", null, Color(0.77, 0.17, 0.17), 0.5, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$HitTween.start()


func set_move(mode):
	can_move = mode

func toggle_shield():
	if Input.is_action_just_pressed("ui_accept") and stamina > 0:
		shield_up = !shield_up
	elif stamina <= 0:
		shield_up = false

func consume_stamina(value):
	$StaminaBar.modulate.a = 1
	stamina = clamp(stamina - value, 0, 100)	

func _on_HitTween_tween_all_completed():
	$HitTweenBack.interpolate_property($AnimatedSprite, "modulate", null, Color(1, 1, 1), 0.8, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	$HitTweenBack.start()

func _on_HealthFade_timeout():
	$HealthBar.modulate.a = lerp($HealthBar.modulate.a, 0, 0.10)

func _on_DashTween_tween_all_completed():
	can_move = true
	
func _on_Shield_consume_stamina(value):	
	consume_stamina(value)

func _on_StaminaFade_timeout():
	$StaminaBar.modulate.a = lerp($StaminaBar.modulate.a, 0, 0.10)


func _on_StaminaRecovery_timeout():
	if !shield_up:
		consume_stamina(-0.5)


func _on_DashTimer_timeout():
	can_dash = false
	$RespawnDash.start()


func _on_RespawnDash_timeout():
	can_dash = true
	dashing = false
