extends Area2D

onready var screen_shake = get_tree().current_scene.get_node("Player/Camera2D/ScreenShake")

func _ready():
	$AnimationTween.interpolate_property(self, "scale", null, Vector2(2.5, 2), 0.2, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
	$AnimationTween.start()
	$AnimatedSprite.play("explode")


func get_random_int(min_value, max_value) -> int:
	randomize()
	return (randi() % max_value) + min_value

func _on_DropZone_body_entered(body):
	if body.name == "Player":
		var player = get_tree().current_scene.get_node("Player")
		player.hitted(get_random_int(15, 25))


func _on_AutoDestruct_timeout():
	self.queue_free()


func _on_AnimationTween_tween_all_completed():
	$AutoDestruct.start()
	screen_shake.start()
	

