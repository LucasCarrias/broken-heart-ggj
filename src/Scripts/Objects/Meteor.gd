extends Node2D

const DROPZONE = preload("res://src/Scenes/Objects/DropZone.tscn")
	

func drop(start_pos: Vector2):
	global_position = start_pos
	$Tween.interpolate_property(self, "global_position", global_position, Vector2(position.x, position.y + 500), 1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()

func _on_Tween_tween_all_completed():
	var explosion = DROPZONE.instance()
	get_tree().current_scene.add_child(explosion)
	explosion.position = self.position
	queue_free()
