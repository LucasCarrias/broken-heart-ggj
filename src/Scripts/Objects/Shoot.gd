extends Node2D

onready var drop_mode setget set_drop

func shoot():
	if drop_mode:
		$AnimatedSprite.play("DropExplosion")
		$AnimatedSprite.frame = 0
		$AnimatedSprite.visible = true
	else:
		$Sprite.visible = true
	$AutoDestruct.start()

func set_drop(mode):
	drop_mode = mode


func _on_AutoDestruct_timeout():
	self.queue_free()


func _on_HitBox_body_entered(body):
	if body.name == "Player":
		get_tree().current_scene.get_node("Player").hitted(15)
