extends Node2D

func drop(pos, explosion_size):
	self.position = pos
	self.scale = explosion_size
	$AnimatedSprite.play("Drop")
	$AutoDestruct.start()

func _on_AutoDestruct_timeout():
	self.queue_free()


func _on_HitBox_body_entered(body):
	if body.is_in_group("Boss"):
		get_tree().current_scene.get_node("NPCs/Boss")
		
