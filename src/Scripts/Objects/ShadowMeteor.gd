extends Node2D

func _ready():
	$AutoDestruct.start()
	$Tween.interpolate_property(self, "scale", null, Vector2(scale.x + 8, scale.y + 8), 1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()

func _on_AutoDestruct_timeout():
	self.queue_free()
