extends Node
class_name CutsceneLibrary

signal CutsceneFinished

onready var player = get_node("../Player")
onready var player_anim = get_node("../Player/Sprite/AnimationPlayer")
onready var camera = get_node("../Player/Camera2D")
onready var dialogBox = get_node("HUD/DialogBox")
onready var tween = get_node("Tween")
onready var cam_tween = get_node("CameraTween")
onready var last_direction = ""

func move_player(direction: String, distance: float, time: float) -> void:
	last_direction = direction
	match direction:
		"up":
			player_anim.play("WalkUp")
			tween.interpolate_property(player, "position", player.position, Vector2(player.position.x, player.position.y - distance), time, Tween.TRANS_SINE, Tween.EASE_OUT)
		"down":
			player_anim.play("WalkDown")
			tween.interpolate_property(player, "position", player.position, Vector2(player.position.x, player.position.y + distance), time, Tween.TRANS_SINE, Tween.EASE_OUT)
		"left":
			player_anim.play("WalkLeft")
			tween.interpolate_property(player, "position", player.position, Vector2(player.position.x - distance, player.position.y), time, Tween.TRANS_SINE, Tween.EASE_OUT)
		"right":
			player_anim.play("WalkRight")
			tween.interpolate_property(player, "position", player.position, Vector2(player.position.x + distance, player.position.y), time, Tween.TRANS_SINE, Tween.EASE_OUT)

	tween.start()


func camera_zoom_out(time, modifier):
	cam_tween.interpolate_property(camera, "zoom", null, Vector2(camera.zoom.x + modifier, camera.zoom.y + modifier), time, Tween.TRANS_SINE, Tween.EASE_OUT)
	cam_tween.start()

func camera_zoom_in(time, modifier):
	cam_tween.interpolate_property(camera, "zoom", null, Vector2(camera.zoom.x - modifier, camera.zoom.y - modifier), time, Tween.TRANS_SINE, Tween.EASE_OUT)	
	cam_tween.start()

func set_user_control(mode) -> void:
	player.set_move(mode)
	player.get_node("HUD/Joystick").visible = false


func set_idle_frame() -> void:
	var sprite = player.get_node("Sprite")
	match last_direction:
		"up":
			sprite.frame = 12
		"down":
			sprite.frame = 0
		"left":
			sprite.frame = 4
		"right":
			sprite.frame = 8
	player_anim.stop()

