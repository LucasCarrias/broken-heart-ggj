extends Polygon2D

onready var Text = get_node("RichTextLabel")
onready var player = get_parent().get_parent()
onready var typing = false
onready var message = "=============="
onready var current_index = 0
onready var allow_escape = false
onready var simple_mode = false
onready var cutscene_mode = false

func _ready():
	set_process(true)


func _process(delta):
	if Input.is_action_just_pressed("ui_accept") and typing:
		Text.set_visible_characters(len(message))


func show_simple_message(msg):
	simple_mode = true
	message = msg
	Text.set_visible_characters(0)
	Text.set_bbcode(message)
	self.show()
	$Timer.start()


func show_sentences(sentences):
	var current_index = 0
	if current_index < len(sentences):
		start(sentences[current_index])
		current_index += 1
		#player.set_move(false)
	else:
		pass

func start(msg):
	message = msg
	Text.set_visible_characters(0)
	Text.set_bbcode(message)
	self.show()
	$Timer.start()


func _on_Timer_timeout():
	if Text.visible_characters < len(message):
		Text.visible_characters += 1
		typing = true
	else:
		typing = false
		$Timer.stop()
		if simple_mode:
			$HideTimer.start()


func _on_HideTimer_timeout():
	simple_mode = false
	self.hide()
	if not cutscene_mode: player.set_move(true)
