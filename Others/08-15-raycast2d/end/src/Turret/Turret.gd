extends StaticBody2D

signal player_found
var can_shoot:= true
var can_ram := true


export var rotate_speed:= 90.0
export var turret_shot: PackedScene

onready var pointer:= $Pointer
onready var ray:= $Pointer/RayCast2D
onready var shot_timer:= $ShotTimer

var target_pos = Vector2.ZERO

func _ready():
	pass # Replace with function body.


func _process(delta):
	pointer.rotation_degrees += rotate_speed * delta
	shoot()


func shoot()->void:
	if can_shoot and ray.is_colliding():
		if ray.get_collider().is_in_group("Player"):
			target_pos = ray.get_collision_point()
			if can_ram:
				can_ram = false
				emit_signal("player_found", 
							self.global_position, target_pos, 
							pointer.rotation_degrees)
				can_shoot = false
				shot_timer.start()
				$RamTimer.start()
			var temp = turret_shot.instance()
			add_child(temp)
			temp.global_position = global_position
			temp.rotation_degrees = pointer.rotation_degrees + 90
			temp.setup(ray.get_collision_point() - position)


func _on_ShotTimer_timeout():
	can_shoot = true



func _on_RamTimer_timeout():
	$Ram.interpolate_property(self, "global_position", null, target_pos, 1,Tween.TRANS_BACK,Tween.EASE_IN_OUT)
	$Ram.start()


func _on_Ram_tween_all_completed():
	can_ram = true
