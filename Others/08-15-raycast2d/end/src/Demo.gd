extends Node2D

export var hit_effect: PackedScene


func generate_hit_effect(hit_position: Vector2)->void:
	var temp = hit_effect.instance()
	add_child(temp)
	temp.position = hit_position


func _on_Player_fired_shot(hit_position: Vector2):
	generate_hit_effect(hit_position)


func _on_Turret_player_found(source_pos: Vector2, target_pos : Vector2, angle):	
	var img = preload("res://assets/misc/Control.tscn").instance()
	img.rect_position = source_pos - Vector2(20,0)
	img.rect_size.x = source_pos.distance_to(target_pos)
	img.rect_rotation = angle
	add_child(img)
